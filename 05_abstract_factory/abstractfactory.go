package abstract_factory

import "fmt"

type GoodsMain interface {
	SaveGoodsMain()
}

type GoodsDetail interface {
	SaveGoodsDetail()
}

type GoodsFactory interface {
	CreateGoodsMain() GoodsMain
	CreateGoodsDetail() GoodsDetail
}

type HotGoodsMain struct {
}

func (*HotGoodsMain) SaveGoodsMain() {
	fmt.Println("hot save goods main")
}

type HotGoodsDetail struct {
}

func (*HotGoodsDetail) SaveGoodsDetail() {
	fmt.Println("hot save goods main")
}

type HotGoodsFactory struct {
}

func (*HotGoodsFactory) CreateGoodsMain() GoodsMain {
	return &HotGoodsMain{}
}

func (*HotGoodsFactory) CreateGoodsDetail() GoodsDetail {
	return &HotGoodsDetail{}
}

type NormalGoodsMain struct {
}

func (*NormalGoodsMain) SaveGoodsMain() {
	fmt.Println("normal save goods main")
}

type NormalGoodsDetail struct {
}

func (*NormalGoodsDetail) SaveGoodsDetail() {
	fmt.Println("normal save goods main")
}

type NormalGoodsFactory struct {
}

func (*NormalGoodsFactory) CreateGoodsMain() GoodsMain {
	return &NormalGoodsMain{}
}

func (*NormalGoodsFactory) CreateGoodsDetail() GoodsDetail {
	return &NormalGoodsDetail{}
}
