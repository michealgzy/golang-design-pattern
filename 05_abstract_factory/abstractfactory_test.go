package abstract_factory

import "testing"

func GetMainAndDetail(f GoodsFactory) {
	f.CreateGoodsMain().SaveGoodsMain()
	f.CreateGoodsDetail().SaveGoodsDetail()
}

func TestHotFactory(t *testing.T) {
	var factory GoodsFactory
	factory = &HotGoodsFactory{}
	GetMainAndDetail(factory)
}
func TestNormalFactory(t *testing.T) {
	var factory GoodsFactory
	factory = &NormalGoodsFactory{}
	GetMainAndDetail(factory)
}
