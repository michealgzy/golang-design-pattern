package proxy

type Subject interface {
	Do() string
}

type RealSubject struct {
}

func (r *RealSubject) Do() string {
	return "real"
}

type Proxy struct {
	real RealSubject
}

func (p *Proxy) Do() string {
	var res string
	//before do
	res = "before + " + res

	res += p.real.Do()

	// after do
	res = res + " + after"

	return res
}
