package proxy

import (
	"log"
	"testing"
)

func TestProxy(t *testing.T) {
	var s Subject

	s = &Proxy{}

	res := s.Do()
	if res != "before + real + after" {
		log.Fatal()
		t.Fail()
	}
}
