package factory_method

import "testing"

func compute(factory NumberFactory, a int64, b int64) int64 {
	op := factory.Create()
	op.SetA(a)
	op.SetB(b)
	return op.Result()
}

func TestNumber(t *testing.T) {
	var factory NumberFactory

	factory = NumberOperateAddFactory{}
	if compute(factory, 6, 2) != 8 {
		t.Fatal("error with factory method pattern")
	}

	factory = NumberOperateSubFactory{}
	if compute(factory, 8, 3) != 5 {
		t.Fatal("error with factory method pattern")
	}
}
