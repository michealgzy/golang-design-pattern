package factory_method

type Number interface {
	SetA(int64)
	SetB(int64)
	Result() int64
}

type NumberFactory interface {
	Create() Number
}

type NumberOrigin struct {
	a, b int64
}

func (n *NumberOrigin) SetA(a int64) {
	n.a = a
}
func (n *NumberOrigin) SetB(b int64) {
	n.b = b
}

type NumberOperateAddFactory struct{}

func (NumberOperateAddFactory) Create() Number {
	return &Add{
		NumberOrigin: &NumberOrigin{},
	}
}

type Add struct {
	*NumberOrigin
}

func (n Add) Result() int64 {
	return n.a + n.b
}

type NumberOperateSubFactory struct{}

func (NumberOperateSubFactory) Create() Number {
	return &Sub{
		NumberOrigin: &NumberOrigin{},
	}
}

type Sub struct {
	*NumberOrigin
}

func (s Sub) Result() int64 {
	return s.a - s.b
}
