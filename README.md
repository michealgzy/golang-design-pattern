# golang-design-pattern

#### 介绍
golang-设计模式学习

#### 软件架构
dirname
    readme.md
    file.go
    file_test.go


#### 安装教程

1.  git clone https://gitee.com/michealgzy/golang-design-pattern.git
2.  cd [dir]
3.  go test



#### 使用说明

1.  golang学习设计模式


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feature_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 欢迎补充
