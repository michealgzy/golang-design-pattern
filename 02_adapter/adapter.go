package adapter

type PC interface {
	Read() string
}

func NewStorageCard() StorageCard {
	return &SDCard{}
}

type StorageCard interface {
	Storage() string
}

type SDCard struct {
}

func (*SDCard) Storage() string {
	return "storage info"
}

func NewCardReader(s StorageCard) PC {
	return &CardReader{StorageCard: s}
}

type CardReader struct {
	StorageCard
}

func (c *CardReader) Read() string {
	return c.Storage()
}
