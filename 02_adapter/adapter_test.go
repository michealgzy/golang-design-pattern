package adapter

import "testing"

var expect = "storage info"

func TestCardReader(t *testing.T) {
	card := NewStorageCard()
	reader := NewCardReader(card)
	res := reader.Read()
	if res != expect {
		t.Fatalf("expect: %s, actual: %s", expect, res)
	}

}
