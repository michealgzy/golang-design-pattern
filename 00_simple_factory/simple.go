package simple_factory

import "fmt"

type Animal interface {
	Say(i string) string
}

func AnimalCreate(t string) Animal {
	if t == "dog" {
		return &dog{}
	} else if t == "cat" {
		return &cat{}
	} else {
		return nil
	}
}

type dog struct{}

func (d dog) Say(i string) string {
	return fmt.Sprintf("Dog say: %s", i)
}

type cat struct{}

func (c cat) Say(i string) string {
	return fmt.Sprintf("Cat say: %s", i)
}
