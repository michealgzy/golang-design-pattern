package simple_factory

import (
	"testing"
)

func TestDog(t *testing.T) {
	dog := AnimalCreate("dog")
	say := dog.Say("wang")
	if say != "Dog say: wang" {
		t.Fatal("test failed")
	}

}

func TestCat(t *testing.T) {
	cat := AnimalCreate("cat")
	say := cat.Say("miao")
	if say != "Cat say: miao" {
		t.Fatal("test failed")
	}
}
