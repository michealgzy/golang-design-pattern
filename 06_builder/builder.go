package builder

type Run interface {
	Step1()
	Step2()
	Step3()
}

type Person struct {
	Move Run
}

func NewPerson(r Run) *Person {
	return &Person{Move: r}
}

func (p *Person) Construct() {
	p.Move.Step1()
	p.Move.Step2()
	p.Move.Step3()
}

type Man struct {
	result string
}

func (m *Man) Step1() {
	m.result += "1"
}

func (m *Man) Step2() {
	m.result += "2"
}

func (m *Man) Step3() {
	m.result += "3"
}

func (m *Man) GetResult() string {
	return m.result
}

type Women struct {
	result string
}

func (w *Women) Step1() {
	w.result += "1"
}

func (w *Women) Step2() {
	w.result += "2"
}

func (w *Women) Step3() {
	w.result += "3"
}

func (w *Women) GetResult() string {
	return w.result
}
