package builder

import "testing"

func TestMan(t *testing.T) {
	man := &Man{}
	m := NewPerson(man)
	m.Construct()
	result := man.GetResult()
	if result != "123" {
		t.Fatalf("not except string")
	}
}

func TestWomen(t *testing.T) {
	women := &Women{}
	m := NewPerson(women)
	m.Construct()
	result := women.GetResult()
	if result != "123" {
		t.Fatalf("not except string")
	}
}
