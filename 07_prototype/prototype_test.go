package prototype

import (
	"bytes"
	"encoding/gob"
	"log"
	"testing"
)

var pm *PrototypeManager

type Type1 struct {
	Name string
	W    string
}

func (t *Type1) Clone() Cloneable {
	tc := *t
	return &tc
	//return t
}

//type Type2 struct {
//	age uint64
//}
//
//func (t *Type2)Clone() Cloneable {
//	tc := *t
//	return &tc
//
//}

func TestClone(t *testing.T) {
	t1 := pm.Get("t1")

	t2 := t1.Clone()
	log.Println(t1, t2)

	if t1 == t2 {
		t.Fatal("error! get clone not working")
	}
}

func TestCloneFromManger(t *testing.T) {
	c := pm.Get("t1").Clone()
	v := c.(*Type1)
	if v.Name != "type1" {
		t.Fatal("error")
	}
}

func init() {
	pm = NewPrototypeManage()

	t1 := &Type1{
		Name: "aaa",
		W:    "dafadfja",
	}
	log.Println(&t1)

	pm.Set("t1", t1)

}

func deepCopy(dst, src interface{}) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(src); err != nil {
		return err
	}
	return gob.NewDecoder(bytes.NewBuffer(buf.Bytes())).Decode(dst)
}
