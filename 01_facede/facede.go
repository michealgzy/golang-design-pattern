package facede

import "fmt"

// facade interface of facade package
type Animal interface {
	Say() string
}

//new animal
func NewAnimal() Animal {
	return &animal{
		cat: NewCat(),
		dog: NewDog(),
	}
}

type animal struct {
	cat Cat
	dog Dog
}

func (a *animal) Say() string {
	wang := a.dog.SayWang()
	miao := a.cat.SayMiao()
	return fmt.Sprintf("%s\n%s", wang, miao)
}

type Cat interface {
	SayMiao() string
}

type cat struct {
}

func (*cat) SayMiao() string {
	return "cat is saying"
}

func NewCat() Cat {
	return &cat{}
}

type Dog interface {
	SayWang() string
}

type dog struct {
}

func (*dog) SayWang() string {
	return "dog is saying"
}

func NewDog() Dog {
	return &dog{}
}
