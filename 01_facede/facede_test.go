package facede

import "testing"

var expect = "dog is saying\ncat is saying"

// TestFacadeAnimalObj ...
func TestFacadeAnimalObj(t *testing.T) {
	animal := NewAnimal()
	ret := animal.Say()
	if ret != expect {
		t.Fatalf("expect %s, return %s", expect, ret)
	}
}

func BenchmarkNewAnimal(b *testing.B) {
	for i := 0; i < b.N; i++ {
		animal := NewAnimal()
		ret := animal.Say()
		if ret != expect {
			b.Fatalf("expect %s, return %s", expect, ret)
		}
	}
}
