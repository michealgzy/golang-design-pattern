package singleton

import (
	"sync"
)

//单例类
type Singleton struct{}

var singleton *Singleton
var once sync.Once

//获取单例实例
func GetInstance() *Singleton {
	once.Do(func() {
		singleton = &Singleton{}
	})
	return singleton
}
