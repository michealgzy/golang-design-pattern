package singleton

import (
	"log"
	"sync"
	"testing"
	"time"
)

const parCount = 10

func TestSingleton(t *testing.T) {
	ins1 := GetInstance()
	ins2 := GetInstance()
	ins3 := GetInstance()
	log.Println(&ins1, &ins2, &ins3)
	if ins1 != ins2 {
		t.Fatalf("instance is not the same one")
	}
}

func TestParallelSingleton(t *testing.T) {
	start := make(chan struct{})
	wg := sync.WaitGroup{}
	wg.Add(parCount)

	instance := [parCount]*Singleton{}
	for i := 0; i < parCount; i++ {
		go func(index int) {
			//协程阻塞，等待channel被关闭才能继续运行
			<-start
			instance[index] = GetInstance()
			wg.Done()
		}(i)
	}

	//关闭channel，所有协程同时开始运行，实现并行(parallel)
	close(start)
	wg.Wait()
	time.Sleep(time.Second * 3)
	for i := 1; i < parCount; i++ {
		if instance[i] != instance[i-1] {
			t.Fatalf("instance is not the same one")
		}
	}

}
